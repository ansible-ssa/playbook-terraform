# playbook-terraform

Playbook to deploy or destroy Terraform plans.

```bash
# load your AWS credentials as environment variables
. /path/to/env
ansible-navigator run playbook-terraform.yml
# destroy with extra_var
ansible-navigator run playbook-terraform.yml -e terraform_state=absent
```

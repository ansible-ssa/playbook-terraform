terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }

    aap = {
      source = "ansible/aap"
    }

  }
  required_version = ">= 1.2.0"


  backend "s3" {
    bucket = "cjung-terraform"
    key    = "terraform_state/web"
    region = "eu-central-1"
  }

}

provider "aws" {
  region = "eu-central-1"
}

data "aws_ami" "rhel9" {
  most_recent = true

  filter {
    name   = "name"
    values = ["RHEL-9.4.0_HVM-*x86_64*"]
  }
}

resource "aws_vpc" "my_vpc" {
  cidr_block           = "172.31.1.0/24"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name        = "my_vpc"
    Provisioner = "terraform"
  }
}

resource "aws_internet_gateway" "web" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name        = "web"
    Provisioner = "terraform"
  }
}

resource "aws_route_table" "web" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.web.id
  }

  tags = {
    Name        = "web"
    Provisioner = "terraform"
  }
}

resource "aws_security_group" "web" {
  name   = "websg"
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name        = "websg"
    Provisioner = "terraform"
  }
}

resource "aws_vpc_security_group_ingress_rule" "ssh" {
  security_group_id = aws_security_group.web.id

  cidr_ipv4   = "0.0.0.0/0"
  ip_protocol = "tcp"
  from_port   = "22"
  to_port     = "22"
}

resource "aws_vpc_security_group_egress_rule" "web" {
  security_group_id = aws_security_group.web.id

  cidr_ipv4   = "0.0.0.0/0"
  ip_protocol = "-1"

}

resource "aws_subnet" "my_subnet" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = "172.31.1.0/24"
  map_public_ip_on_launch = true

  tags = {
    Name        = "my_subnet"
    Provisioner = "terraform"
  }
}

resource "aws_network_interface" "my_nic" {
  subnet_id       = aws_subnet.my_subnet.id
  security_groups = [aws_security_group.web.id]

  tags = {
    Name        = "my_nic"
    Provisioner = "terraform"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.web.id
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.rhel9.id
  instance_type = "t3.micro"
  key_name      = "ansible-ssa"

  network_interface {
    network_interface_id = aws_network_interface.my_nic.id
    device_index         = 0
  }


  tags = {
    Name        = "rhel9"
    Provisioner = "terraform"
  }
}

resource "aws_ec2_instance_state" "web" {
  instance_id = aws_instance.web.id
  state       = "running"

}
provider "aap" {
  host                 = "https://${var.CONTROLLER_HOST}"
  username             = var.CONTROLLER_USERNAME
  password             = var.CONTROLLER_PASSWORD
  insecure_skip_verify = true
}

resource "aap_inventory" "terraform" {
  name = "terraform"
}

resource "aap_host" "web" {
  inventory_id = aap_inventory.terraform.id
  name         = "web"
  variables = jsonencode({
    terraform_instance  = aws_instance.web
    terraform_state     = aws_ec2_instance_state.web
    terraform_interface = aws_network_interface.my_nic
    terraform_vpc       = aws_vpc.my_vpc
    }
  )
}
